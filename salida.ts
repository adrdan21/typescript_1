//Taller TypeScript
//Adrian Galindres Rosero & Jhonatan Lopez
//Facultad de Ingenieria y Ciencias Basicas
//Ingenieria de Sistemas
//Electiva Profesional II
import {Client} from 'https://deno.land/x/mysql/mod.ts';

const client = await new Client().connect({
  hostname: 'localhost',
  username: 'root',
  db: 'db_typescript_1',
  password: '',
  port: 3306,
})
//Variables
let nombre = "";
let apellido = "";
let celular = "";
let email = "";
let psw = "";
let msg = "";
var aux_validacion = true;
var aux_validacion2 = true;
var aux_validacion3 = true;
var aux_validacion4 = true;
const dts = ["nombre","apellido","celular","email","psw"];
let ssql: {toString: any;} 
let id_usuario;

var pantallaPrincipal = [];
var menuOpcion = "";
let opcSel;
while (opcSel !== 5) {
    pantallaPrincipal[0] = "Taller TypeScript";
    pantallaPrincipal[1] = "1. Listar Usuarios";
    pantallaPrincipal[2] = "2. Insertar Usuarios";
    pantallaPrincipal[3] = "3. Editar Usuarios";
    pantallaPrincipal[4] = "4. Eliminar Usuarios";
    pantallaPrincipal[5] = "5. Salir";

    for (let i = 0; i<pantallaPrincipal.length; i++){
        console.log(pantallaPrincipal[i]);

    }

    const opcSel = parseInt(prompt("Seleccionar Opc") as string)

    const usuarios = await client.execute('select * from usuarios');

    switch (opcSel) {
        case 1:
            console.log(usuarios.rows);
            break;
        case 2:
            nombre = prompt("Nombre usuario: ") as string
            apellido = prompt("Apellido usuario: ") as string
            celular = prompt("Celular usuario: ") as string
            email = prompt("Email usuario: ") as string
            psw = prompt("Contraseña usuario: ") as string

            aux_validacion = validar_nomape(nombre, apellido)
            aux_validacion2 = validar_cel(celular)
            aux_validacion3 = validar_email(email)
            aux_validacion4 = validar_key(psw)

            if(aux_validacion !== false || aux_validacion2 !== false || aux_validacion3 !== false || aux_validacion4 !== false){
                ssql = await client.execute(`INSERT INTO usuarios(nombre, apellido, celular, email, psw) values(?, ?, ?, ?, ?)`, [
                    nombre, apellido, celular, email, psw]);
                msg = (ssql) ? "Usuario registrado satisfactoriamente" : "Error al registrar usuario";
                console.log(msg)
            }
            break;
        case 3:
            console.log(usuarios.rows);
            id_usuario = parseInt(prompt("Ingrese el id de la persona para actualizar sus datos") as string)
            nombre = prompt("Nombre usuario: ") as string
            apellido = prompt("Apellido usuario: ") as string
            celular = prompt("Celular usuario: ") as string
            email = prompt("Email usuario: ") as string
            psw = prompt("Contraseña usuario: ") as string

            aux_validacion = validar_nomape(nombre, apellido)
            aux_validacion2 = validar_cel(celular)
            aux_validacion3 = validar_email(email)
            aux_validacion4 = validar_key(psw)

            if(aux_validacion !== false || aux_validacion2 !== false || aux_validacion3 !== false || aux_validacion4 !== false){
            ssql = await client.execute(`update usuarios set nombre = ?, apellido = ?, celular = ?, email = ?, psw = ? WHERE id = ? `, [
                nombre, apellido, celular, email, psw, id_usuario]);
                msg = (ssql) ? "Usuario editado satisfactoriamente" : "Error al editar usuario";
                console.log(msg)
            }
            break;
        case 4:
            console.log(usuarios.rows);
            id_usuario = parseInt(prompt("Ingrese el id de la persona para eliminar registro") as string)
            ssql = await client.execute(`delete from usuarios where ?? = ?`, ["id", id_usuario])
            msg = (ssql) ? "Usuario eliminado satisfactoriamente" : "Error al eliminar usuario";
            console.log(msg)
            break;
    }
}

function validar_nomape(row1?:string , row2?:string){
    if (!row1 || !row2){
        console.log("El campo Nombre y Apellido es obligatorio")
        return false;
    }else if(row1.length>3 && row2.length > 3){
        return true
    }else{
        console.log("El campo Nombre y Apellido debe tener mas de 3 letras")
        return false
    }
} 

function validar_cel(row:string){
    if (!row){
        console.log("El campo Celular es obligatorio ")
        return false;
    }else if(row.length==10){
        return true;
    }else{
        console.log("El campo Celular debe tener 10 digitos")
        return false;
    }
}
function validar_email(row:string){
    if(!row){
        console.log("El campo Email es obligatorio")
        return false
    }else if(/^\w+([\.-]?\w+)*@(?:|hotmail|outlook|yahoo|live|gmail)\.(?:|com|es)+$/.test(row)){
        return true
    }else{
        console.log("Debe ingresar un correo válido (Hotmail, Outlook, Yahoo, Live, Gmail") 
        return false   
    }
}

function validar_key(row:string){
    if (!row){
        alert("La clave es obligatoria")
        return false
    }else if(row.length>8 && /^.*[A-ZÁÉÍÓÚÜÑ].*[!,%,&,@,#,$,^,*,?,_,~]/.test(row)){
        return true
        
    }else if(row.length<8){
        alert("La clave debe tener mas de 8 caracteres")
        return false
    }else{
        alert("La clave debe tener una mayuscula y un caracter especial")
        return false
    }
}











